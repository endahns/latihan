# -*- coding: utf-8 -*-

from odoo import models, fields, api


class Latihan(models.Model):
    _inherit = 'latihan.latihan'
    
    def update_old_data(self):
        self.browse(self.env.ref('latihan.contoh_latihan1').id).write({
                'name' : 'contoh latihan 1 baru'
            })
        
    @api.onchange('name')
    def check_name(self):
        if self.name:
            if len(self.name) < 3:
                return{
                    'warning' : {
                        'title' : 'Name Warning',
                        'message' : 'Name must be more than 3 char'                    
                    }                
                }
