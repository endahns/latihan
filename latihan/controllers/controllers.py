# -*- coding: utf-8 -*-
from odoo import http
import json
 
class Latihan(http.Controller):
    @http.route('/latihan/latihan/', auth='user')
    def index(self, **kw):
        return "Hello, %s" % (kw.get('name', 'world'))
    
    @http.route('/latihan2/latihan22/', auth='public', website=True)
    def index2(self, **kw):
        who = kw.get('name', 'world')
        return http.request.render('latihan.latihan_template', {
            'who':who
        })
    
    @http.route('/lat/lat/<model("latihan.latihan"):lat>', auth='public')
    def latihan_model(self, lat, **kw):
        return json.dumps(lat.read())
# class Latihan(http.Controller):
#     @http.route('/latihan/latihan/', auth='public')
#     def index(self, **kw):
#         return "Hello, world"

#     @http.route('/latihan/latihan/objects/', auth='public')
#     def list(self, **kw):
#         return http.request.render('latihan.listing', {
#             'root': '/latihan/latihan',
#             'objects': http.request.env['latihan.latihan'].search([]),
#         })

#     @http.route('/latihan/latihan/objects/<model("latihan.latihan"):obj>/', auth='public')
#     def object(self, obj, **kw):
#         return http.request.render('latihan.object', {
#             'object': obj
#         })