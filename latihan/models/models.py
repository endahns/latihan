# -*- coding: utf-8 -*-

from odoo import models, fields, api
from datetime import datetime
from odoo.tools.misc import DEFAULT_SERVER_DATETIME_FORMAT
from odoo.exceptions import ValidationError

class Latihan(models.Model):
    _name = 'latihan.latihan'
    _description = 'Latihan'
    _order = 'date_start desc'

    name = fields.Char('Nama', required=True)
    note = fields.Text('Note')
    line_ids = fields.One2many('latihan.line', 'latihan_id', string='Latihan Line')
    option_ids = fields.Many2many('latihan.option', 'opt_relation','latihan_id', 'opt_id' ,string='Latihan Line')
    date_start = fields.Datetime('Tanggal Mulai', required=True, default=lambda self:fields.Datetime.now())
    date_end = fields.Datetime('Tanggal Selesai')
    time_delta = fields.Char('Lama Waktu')
    ref = fields.Reference([
        ('latihan.latihan','Latihan'),
        ('latihan.line','Latihan Line'),
        ('latihan.option','Latihan Option'),
        ], string="Referensi")
    ref_model = fields.Char('Ref Model', compute='_get_ref_model')
    state = fields.Selection([
        ('draft', 'Draft'),
        ('start', 'Start'),
        ('end', 'End'),
        ('cancel', 'Cancel')
        ], default=lambda self:'draft', string='State')
    numb = fields.Integer('Nomor Latihan')    
    
    def change_name2(self):
#         self.env.context['name'] = 'nama'
#         self.env.context.get('nama', False)
        return {
            'type':'ir.actions.act_window',
            'name':'Latihan Wizard',
            'res_model':'latihan.wizard',
            'src_model':'latihan.latihan',
            'view_mode':'form',
            'view_type':'form',
            'target':'new',
            'context':{
                'default_latihan_id':self.id
                }        
        }
    
    def start(self):
        for rec in self:
            if rec.state == 'draft':
                rec.state = 'start'
    
    def end(self):
        for rec in self:
            if rec.state == 'start':
                rec.state = 'end'
                
    def cancel(self):
        for rec in self:
            if rec.state == 'draft':
                rec.state = 'cancel'
                
    def reopen(self):
        for rec in self:
            if rec.state == 'cancel':
                rec.state = 'draft'
                
    @api.depends('ref')
    def _get_ref_model(self):
        for rec in self:
            rec.ref_model = rec.ref and rec.ref._name
            
    @api.onchange('date_start', 'date_end')
    def _get_time_delta(self):
        if self.date_start and self.date_end:
            self.time_delta = '%s' % (datetime.strptime(self.date_end, DEFAULT_SERVER_DATETIME_FORMAT)\
                                        - datetime.strptime(self.date_start, DEFAULT_SERVER_DATETIME_FORMAT))
        else:
            self.time_delta='Sebagian nilai kosong'
    
    @api.constrains('date_start', 'date_end')
    def _check_time(self):
        for rec in self:
            if rec.date_end:
                if rec.date_end < rec.date_start:
                    raise ValidationError('Tanggal Mulai tidak boleh lebih dari Tanggal Selesai')
    
class LatihanLine(models.Model):
    _name = 'latihan.line'
    _description = 'Latihan Line'
    
    name = fields.Char('Deskripsi', required=True)
    latihan_id = fields.Many2one('latihan.latihan', string='Latihan', required=True)
    date_start = fields.Datetime('Tanggal Mulai', related='latihan_id.date_start')
    date_end = fields.Datetime('Tanggal Selesai', related='latihan_id.date_end')
    ref_model = fields.Char('Ref Model 12', related='latihan_id.ref_model')
    
    
class LatihanOption(models.Model):
    _name = 'latihan.option'
    _description = 'Latihan Option'
    
    name = fields.Char('Opsi')
    
class LatihanWizard(models.TransientModel):
    _name = 'latihan.wizard'
    _description = 'Latihan Wizard'
    
    new_name = fields.Char('Nama Baru', required=True)
    old_name = fields.Char('Nama Lama', related='latihan_id.name')
    latihan_id = fields.Many2one('latihan.latihan', string='Latihan', required=True)
    
    def change_name(self):
        for rec in self:
            rec.latihan_id.name = rec.new_name
    
#     value = fields.Integer()
#     value2 = fields.Float(compute="_value_pc", store=True)
#     description = fields.Text()
#
#     @api.depends('value')
#     def _value_pc(self):
#         self.value2 = float(self.value) / 100